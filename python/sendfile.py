import requests
import sys
import json
import os
import datetime

#Verificamos si viene el arguemnto con el archivo
if len(sys.argv) != 2:
	print("Se necesita argumento con path del file a enviar")
	sys.exit(1)

#Definimos variables
url = 'url'
#url = 'http://localhost:5001/smartcred-1/us-central1/smartcred'

#url = os.environ["APIURL"]
user = os.environ["APRESUSER"]
password = os.environ["APRESUSERPASS"]
filepath = sys.argv[1]



if os.path.isfile(filepath) != True:
	print("Archivo no existe")
	sys.exit(1)



print(datetime.datetime.now())

payload = {
    'email': user,
    'password': password,
}

headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url + '/api/v1/login/apres', headers=headers, data = payload)

errors = response.json()["error"] 
if errors == 0:
	print("###### Login ok ######")
	mytoken = response.json()["token"]
elif errors != 0:
	print("Algo anda mal en el login")
	print(response.text.encode('utf8'))
	sys.exit(1)

######################################

data = payload
archivo = [
 ('turnos', open(filepath,'rb'))
]
headers = {
  'Authorization': 'Bearer ' + mytoken,
}

subida = requests.request("POST", url + '/api/v1/turnos/apres', headers=headers, data = payload, files = archivo)

errors = subida.status_code
#error = subida.json()["error"]
#print(subida.json())
statusCode = subida.status_code
#print(subida.text)
#sys.exit()

if errors == 200:
    print("Respuesta ok")
#	respuestafile = subida.json()["file"]
#	print(respuestafile)
    print(subida.text)
elif errors != 200:
    print("Algo anda mal")
    print("Status code: " + str(statusCode))
    print(subida.text)
    sys.exit(1)

