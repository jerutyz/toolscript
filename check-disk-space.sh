#!/bin/bash
## ADD to crontab
#12 12 * * * /root/toolscript/./check-disk-space.sh

# set admin email 
ADMIN="it@itcsoluciones.com"
# set alert level 88% 
ALERT=88
df -H | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $5 " " $1 }'|grep dev |while read output
do
	usep=$(echo $output | cut -d'%' -f1)
	partition=$(echo $output|awk '{ print $2 }')

if [ $usep -gt $ALERT ]
then
	MAIL_TXT="Subject: Disco Alerta $(hostname) \n\n $usep % en $partition"
	echo -e $MAIL_TXT | /usr/sbin/sendmail "$ADMIN"
  fi


done
